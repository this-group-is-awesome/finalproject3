/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.krorawitt.finalproject.Testselection;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Employee;

/**
 *
 * @author cherz__n
 */
public class EmployeeDAO implements DAO_Interface<Employee> {

    @Override
    public int add(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO Emp (EmpName,EmpPhone,EmpSector)VALUES (?,?,? )";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getEmpName());
            stmt.setString(2, object.getEmpTelephone());
            stmt.setString(3, object.getEmpPosition());

            int row = stmt.executeUpdate();
            System.out.println("Affect row " + row);
            ResultSet result = stmt.getGeneratedKeys();

            if (result.next()) {
                id = result.getInt(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return id;
    }

    @Override
    public ArrayList<Employee> getAll() {
        ArrayList list = new ArrayList();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();

        try {
            String sql = "SELECT EmpID,EmpName,EmpPhone,EmpSector FROM Emp";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            while (result.next()) {
                int EmpID = result.getInt("EmpID");
                String EmpName = result.getString("EmpName");
                String EmpkTelephone = result.getString("EmpPhone");
                String EmpPosition = result.getString("EmpSector");
                Employee Emp = new Employee(EmpID, EmpName, EmpkTelephone, EmpPosition);
                list.add(Emp);

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return list;
    }

    @Override
    public Employee get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT EmpID,EmpName,EmpPhone,EmpSector FROM Emp WHERE EmpID = ?" + id;
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);
            if (result.next()) {
                int EmpID = result.getInt("EmpID");
                String EmpName = result.getString("EmpName");
                String EmpkTelephone = result.getString("EmpPhone");
                String EmpPosition = result.getString("EmpSector");
                Employee Emp = new Employee(EmpID, EmpName, EmpkTelephone, EmpPosition);
                return Emp;

            }

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;

    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "DELETE FROM Emp WHERE EmpID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;
    }

    @Override
    public int update(Employee object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;

        try {
            String sql = "UPDATE Emp SET EmpName = ?,EmpPhone = ?,EmpSector = ? WHERE EmpID = ?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, object.getEmpName());
            stmt.setString(2, object.getEmpTelephone());
            stmt.setString(3, object.getEmpPosition());
            stmt.setInt(4, object.getEmpID());
            row = stmt.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(Testselection.class.getName()).log(Level.SEVERE, null, ex);
        }

        db.close();
        return row;

    }

    public static void main(String[] args) {
        EmployeeDAO dao = new EmployeeDAO();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Employee(-1, "Kikada", "gg", "444"));
        System.out.println("id: " + id);
        Employee updateProduct = dao.get(id);
        System.out.println("updateproduct:" + updateProduct);

    }

}
