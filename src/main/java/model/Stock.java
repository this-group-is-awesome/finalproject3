/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

public class Stock {

    private int StockID;
    private String StockName;
    private int StockAmount;
    private double StockPrice;

    public int getStockID() {
        return StockID;
    }

    public void setId(int StockID) {
        this.StockID = StockID;
    }

    public String getStockName() {
        return StockName;
    }

    public void setName(String StockName) {
        this.StockName = StockName;
    }

    public int getStockAmount() {
        return StockAmount;
    }

    public void setStockAmount(int StockAmount) {
        this.StockAmount = StockAmount;
    }

    public double getStockPrice() {
        return StockPrice;
    }

    public void setStockPrice(double StockPrice) {
        this.StockPrice = StockPrice;
    }

    public Stock(int StockID, String StockName, int StockAmount, double StockPrice) {
        this.StockID = StockID;
        this.StockName = StockName;
        this.StockAmount = StockAmount;
        this.StockPrice = StockPrice;
    }

    @Override
    public String toString() {
        return "stock{" + "StockID=" + StockID + ", StockName=" + StockName + ", StockAmount=" + StockAmount + ", StockPrice=" + StockPrice + '}';
    }

}
