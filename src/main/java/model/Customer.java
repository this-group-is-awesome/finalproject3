/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author cherz__n
 */
public class Customer {

    private int id;
    private String name;
    private String phonnum;
    private String cusadd;

    public Customer(int id, String name, String phonnum, String cusadd) {
        this.id = id;
        this.name = name;
        this.phonnum = phonnum;
        this.cusadd = cusadd;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhonnum() {
        return phonnum;
    }

    public void setPhonnum(String phonnum) {
        this.phonnum = phonnum;
    }

    public String getCusadd() {
        return cusadd;
    }

    public void setCusadd(String cusadd) {
        this.cusadd = cusadd;
    }

    @Override
    public String toString() {
        return "custommer{" + "id=" + id + ", name=" + name + ", phonnum=" + phonnum + ", cusadd=" + cusadd + '}';
    }
}
